# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
Then watch the api doc here: https://api.ovh.com/console/#/cloud/project/%7BserviceName%7D/kube#POST
'''
import os
import ovh
import json


zone_name = os.environ['OVH_RECORD_ZONE_NAME']
field_type = os.environ['OVH_RECORD_FIELD_TYPE']
sub_domain = os.environ['OVH_RECORD_SUB_DOMAIN']
target = os.environ['OVH_RECORD_TARGET']
ttl = int(os.environ['OVH_RECORD_TTL'])

ovh_endpoint = os.environ['OVH_ENDPOINT']
ovh_application_key = os.environ['OVH_APPLICATION_KEY']
ovh_application_secret = os.environ['OVH_APPLICATION_SECRET']
ovh_consumer_key = os.environ['OVH_CONSUMER_KEY']

client = ovh.Client(
    endpoint=ovh_endpoint,
    application_key=ovh_application_key,
    application_secret=ovh_application_secret,
    consumer_key=ovh_consumer_key
)


def get_ovh_dns_record(zone_name, field_type, sub_domain):
    result = client.get('/domain/zone/{zoneName}/record'.format(zoneName=zone_name),
                        fieldType=field_type,
                        subDomain=sub_domain,
                        )
    return result


def delete_ovh_dns_record(zone_name, record_id):
    result = client.delete(
        '/domain/zone/{zoneName}/record/{id}'.format(zoneName=zone_name, id=record_id))
    return result


def check_if_record_is_already_at_good_value(zone_name, field_type, sub_domain, target, ttl, records_ids):
    if len(records_ids) == 0:
        return False
    elif len(records_ids) > 1:
        return False

    existing_record = client.get(
        '/domain/zone/{zoneName}/record/{id}'.format(zoneName=zone_name, id=records_ids[0]))

    if existing_record['fieldType'] != field_type:
        return False

    if existing_record['subDomain'] != sub_domain:
        return False

    if existing_record['target'] != target:
        return False

    if existing_record['ttl'] != ttl:
        return False

    return True


def create_or_update_ovh_dns_record(zone_name, field_type, sub_domain, target, ttl):
    records_ids = get_ovh_dns_record(zone_name, field_type, sub_domain)
    print('Existing record with same sub domain ("{}"):'.format(sub_domain))
    print(json.dumps(records_ids, indent=4))

    already_exist = check_if_record_is_already_at_good_value(
        zone_name, field_type, sub_domain, target, ttl, records_ids)
    if already_exist:
        print('The exact same record already exist: Exit')
        return

    if len(records_ids):
        print('Record already exist but needs to be updated: Delete previous records before create')
        for record_id in records_ids:
            delete_ovh_dns_record(zone_name, record_id)

    print('Create record')
    result = client.post(
        '/domain/zone/{zoneName}/record'.format(zoneName=zone_name),
        # Resource record Name(type: zone.NamedResolutionFieldTypeEnum)
        fieldType=field_type,
        # Resource record subdomain(type: string)
        subDomain=sub_domain,
        # Resource record target(type: string)
        target=target,
        ttl=ttl,  # Resource record ttl(type: long)
    )

    # Pretty print
    print(json.dumps(result, indent=4))
    # Response example
    ###########################
    # fieldType: "A"
    # id: 5161284322
    # subDomain: "tiiiiiii"
    # target: "1.1.1.1"
    # ttl: 0
    # zone: "keltio.fr"

    print('Apply all changes in dns')
    result = client.post(
        '/domain/zone/{zoneName}/refresh'.format(zoneName=zone_name))


create_or_update_ovh_dns_record(zone_name, field_type, sub_domain, target, ttl)
