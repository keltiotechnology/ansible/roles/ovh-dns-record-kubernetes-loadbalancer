# Ovh dns record kubernetes loadbalancer

Create record on ovh with the ingress nginx loadbalancer

## Local prerequisites
- pip packages: ovh + requests (see requirements.txt)


## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| secret_ovh_application_key | The ovh application key | `string` | `` | yes |
| secret_ovh_application_secret | The ovh application secret | `string` | `` | yes |
| secret_ovh_consumer_key | The ovh consumer key | `string` | `` | yes |
| ovh_dns_record_zone_name | The ovh zone name | `string` | `` | yes |
| ovh_dns_record_sub_domains | The ovh records to save | `List<record>` | `[]` | yes |
| ovh_dns_record_field_type | Type of record created | `string` | `A` | no |
| ovh_dns_record_ttl | The records ttl | `number` | `3600` | no |
| ovh_endpoint | The ovh endpoint | `string` | `ovh-eu` | no |
| ovh_dns_record_tmp_dir | The tmp dir for the role exec | `string` | `/tmp/ovh/dns` | no |


## Example of vars
```yaml
secret_ovh_application_key: XXXXXX
secret_ovh_application_secret: XXXXXX
secret_ovh_consumer_key: XXXXXX
ovh_dns_record_zone_name: keltio.fr
ovh_dns_record_sub_domains: [nginx]
```
